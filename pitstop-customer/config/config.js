const fs = require('fs');

module.exports = {
  development: {
    username: 'root',
    password: null,
    database: 'database_development',
    host: "customer-db",
    dialect: 'mysql',
    logging: false,
    dialectOptions: {
      bigNumberStrings: true
    }
    },
    test: {
      username: 'root',
      password: null,
      database: 'database_test',
      host: 'customer-db',
      port: 3306,
      dialect: 'mysql',
      dialectOptions: {
        bigNumberStrings: true
      }
    },
    production: {
      username: 'root',
      password: "sekkat",
      database: 'database_production',
      host: 'customer-db',
      port: 3306,
      dialect: 'mysql',
      dialectOptions: {
        bigNumberStrings: true,
      }
    }
  };